# -*- coding: utf-8 -*-
"""
Problemas matematicos resueltos en python :D
1.  Cuantos Coleopteros tiene un coleccionista si tiene 10 insectos entre 
arañas y escarabajos si en total tienen 72 Patitas

Solución:
Los Coleopteros son escarabajos y tiene 6 Patitas, 
las arañas tienen 8 Patitas. 
Insectos igual a 10 y la suma de arañas * 8 más escarabajos * 6 debe ser 72

"""

for i in range(0,10):
    if (i * 6) + ((10 - i) * 8) == 72:
        print ('Tiene %i escarabajos, %i arañas, total %i insectos y %i patitas'
                % (i, 10-i, i + (10-i), (i * 6) + ((10 - i) * 8)))



