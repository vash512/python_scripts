# -*- coding: utf-8 -*-
"""
En C++ y parecidos, para ”ocultar” un método o atributo a otras clases derivadas o a 
instancias de la clase, utilizamos modificadores como {public,private,protected ... } ;
en Python se utiliza algo más sencillo:
__MiAtributoPrivado → atributo privado
def __MétodoPrivado (self,parámetros): → método privado
Si comienza con dos ”__” (guiones bajos) es privado a la propia clase.
"""


class Ejemplo:
    __hola = "hola Privada "
    hola = "hi! :D juju publicamente"
    def publico(self):
        print "hola soy público"
    def __privado(self):
        print "Hola soy privado"

ej = Ejemplo()
ej.publico()
#ej.__privado() #Este metodo falla , porque es privado

#Ejemplo de usar un elemento privado correctamente
ej._Ejemplo__privado()

print ej.hola
#Imprimir Privada
print ej._Ejemplo__hola

#Fuente de información
#http://www.polinux.upv.es/drupal/files/Python_orientacion_objetos_Polinux.pdf